# Changelog

## Solarus Sample Quest 1.6.5 (2021-04-06)

* Update assets from latest Solarus Free Resource Pack.

## Solarus Sample Quest 1.6.4 (2020-04-12)

No change in the Sample Quest package.

## Solarus Sample Quest 1.6.3 (2020-04-11)

* Update assets from latest Solarus Free Resource Pack.

## Solarus Sample Quest 1.6.2 (2019-08-16)

* Fix broken enemies.

## Solarus Sample Quest 1.6.1 (2019-08-10)

* Update assets to Solarus Free Resource Pack 1.6.1.

## Solarus Sample Quest 1.6.0 (2018-12-22)

* Update sample quest to Solarus 1.6.
* Add various resources from Solarus Free Resource Pack.
* Add shader examples.

## Solarus Sample Quest 1.5.2 (2017-04-03)

* Play a sound with the Solarus logo.
* Add crow enemy by Diarandor.
* Add thunder and bird sounds by Diarandor.
* Make scripts more independent to each other.
* Fix moving with the mouse on maps bigger than the camera.

## Solarus Sample Quest 1.5.1 (2016-11-29)

* Add enemies, NPCs, musics and sounds (thanks Diarandor!).
* Add a clickable HUD (thanks Vlag!).
* Put the solarus logo script in scripts/menus/.
* Fix wrong hero sprite after game-over.

## Solarus Sample Quest 1.5.0 (2016-07-27)

* Update sample quest to Solarus 1.5.
* Lots of new sprites and sounds from Diarandor.

## Solarus Sample Quest 1.4.0 (2015-05-02)

* Update sample quest to Solarus 1.4.

## Solarus Sample Quest 1.3.0 (2014-08-21)

* Update sample quest to Solarus 1.3.

## Solarus Sample Quest 1.2.0 (2014-05-06)

* Update sample quest to Solarus 1.2.

## Solarus Sample Quest 1.1.0 (2013-10-13)

* Add a very short sample quest with free graphics and musics (#232, #318).
