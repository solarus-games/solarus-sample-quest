![Sample quest logo](data/logos/logo.png)

# Solarus Sample Quest

**Solarus Sample Quest** is a simple quest aimed to help you bootstrap your own project. It is packaged by default with Solarus. Moreover, it contains only free elements:
 - Scripts are licensed under GPL v3.
 - Assets are licensed under CC-BY-SA 4.0.
 